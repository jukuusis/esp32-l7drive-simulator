// ajax - v2.3.0 - MIT (c) Fernando Daciuk
!function(e,t){"use strict";"function"==typeof define&&define.amd?define("ajax",t):"object"==typeof exports?exports=module.exports=t():e.ajax=t()}(this,function(){"use strict";function e(e){var r=["get","post","put","delete"];return e=e||{},e.baseUrl=e.baseUrl||"",e.method&&e.url?n(e.method,e.baseUrl+e.url,t(e.data),e):r.reduce(function(r,o){return r[o]=function(r,u){return n(o,e.baseUrl+r,t(u),e)},r},{})}function t(e){return e||null}function n(e,t,n,u){var c=["then","catch","always"],i=c.reduce(function(e,t){return e[t]=function(n){return e[t]=n,e},e},{}),f=new XMLHttpRequest,d=r(t,n,e);return f.open(e,d,!0),f.withCredentials=u.hasOwnProperty("withCredentials"),o(f,u.headers),f.addEventListener("readystatechange",a(i,f),!1),f.send(s(n)),i.abort=function(){return f.abort()},i}function r(e,t,n){if("get"!==n.toLowerCase()||!t)return e;var r=s(t),o=e.indexOf("?")>-1?"&":"?";return e+o+r}function o(e,t){t=t||{},u(t)||(t["Content-Type"]="application/x-www-form-urlencoded"),Object.keys(t).forEach(function(n){t[n]&&e.setRequestHeader(n,t[n])})}function u(e){return Object.keys(e).some(function(e){return"content-type"===e.toLowerCase()})}function a(e,t){return function n(){t.readyState===t.DONE&&(t.removeEventListener("readystatechange",n,!1),e.always.apply(e,c(t)),t.status>=200&&t.status<300?e.then.apply(e,c(t)):e["catch"].apply(e,c(t)))}}function c(e){var t;try{t=JSON.parse(e.responseText)}catch(n){t=e.responseText}return[t,e]}function s(e){return i(e)?f(e):e}function i(e){return"[object Object]"===Object.prototype.toString.call(e)}function f(e){return Object.keys(e).reduce(function(t,n){var r=t?t+"&":"";return r+d(n)+"="+d(e[n])},"")}function d(e){return encodeURIComponent(e)}return e});

function debounce(func, delay) {
    var inDebounce;
    return function () {
        var context = this;
        var args = arguments;
        clearTimeout(inDebounce);
        inDebounce = setTimeout(function () {
            func.apply(context, args);
        }, delay);
    };
}
function bind_trailing_args(fn) {
    var bound_args = [].slice.call(arguments, 1);
    return function() {
        var args = [].concat.call([].slice.call(arguments), bound_args);
        return fn.apply(this, args);
    };
}

function linkSlider(sliderID, outputID, callback) {
    var slider = document.getElementById(sliderID);
    var output = document.getElementById(outputID);
    output.innerHTML = slider.value; // Display the default slider value

   // Update the current slider value (each time you drag the slider handle)

    slider.oninput = function() {
        output.innerHTML = this.value;
        if (callback != undefined) { callback(this.value); }
    };
}


function linkSlider1(sliderID, outputID, callback) {

    var values = [1,2,3,4,5,99];
    var slider = document.getElementById(sliderID);
    var output = document.getElementById(outputID);
    output.innerHTML = slider.value; // Display the default slider value

   // Update the current slider value (each time you drag the slider handle)

    slider.oninput = function() {
        output.innerHTML = values[this.value];
        if (callback != undefined) { callback(values[this.value]); }
    };
}


function linkSlider2(sliderID, outputID, callback) {

    var values = ['init 0x00','charging 0x10','active 0x400','chr + active 0x410'];
    var slider = document.getElementById(sliderID);
    var output = document.getElementById(outputID);
    output.innerHTML = slider.value; // Display the default slider value

   // Update the current slider value (each time you drag the slider handle)

    slider.oninput = function() {
        output.innerHTML = values[this.value];
        if (callback != undefined) { callback(values[this.value]); }
    };
}
function updateOBDValue(value, name) {
    console.log('OBD', name, value);

    var data = {
        name: name,
        value: value
    };

    ajax({
        method: 'PATCH',
        url: '/api/vehicle',
        data: data
    });
}


linkSlider('battery_l', 'current-level', debounce(bind_trailing_args(updateOBDValue, 'battery_l'), 100)); // 60/01
linkSlider('capa', 'current-capa', debounce(bind_trailing_args(updateOBDValue, 'capa'), 100)); // 60/02
linkSlider('battery_c', 'current-battery_current', debounce(bind_trailing_args(updateOBDValue, 'battery_c'), 100)); //60/03
linkSlider('battery_v', 'current-battery_voltage', debounce(bind_trailing_args(updateOBDValue, 'battery_v'), 100)); //60/04
linkSlider('system_v', 'current-system-output-voltage', debounce(bind_trailing_args(updateOBDValue, 'system_v'), 100)); //60/05
linkSlider('temperature', 'current-temperature', debounce(bind_trailing_args(updateOBDValue, 'temperature'), 100)); //60/06
linkSlider('heating_p', 'current-heating-battery-power', debounce(bind_trailing_args(updateOBDValue, 'heating_p'), 100)); //60/07
linkSlider('booster_p', 'current-booster-power', debounce(bind_trailing_args(updateOBDValue, 'booster_p'), 100));//60/08
linkSlider('grid_in_p', 'current-input-power-from-grid', debounce(bind_trailing_args(updateOBDValue, 'grid_in_p'), 100)); //60/09
linkSlider('total_output_p', 'current-total-output-power', debounce(bind_trailing_args(updateOBDValue, 'total_output_p'), 100)); //60/0A
linkSlider("bcs_t", 'l7drive-bcs-temperature', debounce(bind_trailing_args(updateOBDValue, 'bcs_t'), 100)); // 60/0C
linkSlider("battery_soh", 'battery-soh', debounce(bind_trailing_args(updateOBDValue, 'battery_soh'), 100)); // 60/0D
linkSlider("ucell1_t", 'ucell1-temperature', debounce(bind_trailing_args(updateOBDValue, 'ucell1_t'), 100)); // 60/0E
linkSlider("ucell2_t", 'ucell2-temperature', debounce(bind_trailing_args(updateOBDValue, 'ucell2_t'), 100)); // 60/0F
linkSlider("ucell3_t", 'ucell3-temperature', debounce(bind_trailing_args(updateOBDValue, 'ucell3_t'), 100)); // 60/10
linkSlider('battery_h', 'battery-heating', debounce(bind_trailing_args(updateOBDValue, 'battery_h'), 100)); //
linkSlider('battery_coo', 'battery-cooling', debounce(bind_trailing_args(updateOBDValue, 'battery_coo'), 100)); //
// linkSlider('battery_c_t', 'battery-cycle-test', debounce(bind_trailing_args(updateOBDValue, 'battery_c_t'), 100)); //
linkSlider('storage_s', 'storage-switch', debounce(bind_trailing_args(updateOBDValue, 'storage_s'), 100)); //
linkSlider('rebooted', 'l7-rebooted', debounce(bind_trailing_args(updateOBDValue, 'rebooted'), 100)); //
linkSlider('ac_grid_o', 'grid-off', debounce(bind_trailing_args(updateOBDValue, 'ac_grid_o'), 100)); //
linkSlider('battery_conn', 'battery-connected', debounce(bind_trailing_args(updateOBDValue, 'battery_conn'), 100)); //
// linkSlider('op_state_n', 'op-status-normal', debounce(bind_trailing_args(updateOBDValue, 'op_state_n'), 100)); //
// linkSlider('op_state_u', 'op-status-ups', debounce(bind_trailing_args(updateOBDValue, 'op_state_u'), 100)); //

// linkSlider('maxinput_p', 'max-input-grid-power', debounce(bind_trailing_args(updateOBDValue, 'maxinput_p'), 100)); // 62/01
// linkSlider('maxbatterychrg_p', 'max-battery-chrg-power', debounce(bind_trailing_args(updateOBDValue, 'maxbatterychrg_p'), 100)); // 62/02
// linkSlider("min_chrg_l", 'operating-min-charge-level', debounce(bind_trailing_args(updateOBDValue, 'min_chrg_l'), 100)); // 62/03
// linkSlider("max_chrg_l", 'operating-max-charge-level', debounce(bind_trailing_args(updateOBDValue, 'max_chrg_l'), 100)); // 62/04
linkSlider("energy_g_w", 'energy-from-grid', debounce(bind_trailing_args(updateOBDValue, 'energy_g_w'), 100)); // 62/05
linkSlider("energy_batt_w", 'energy-from-batt', debounce(bind_trailing_args(updateOBDValue, 'energy_batt_w'), 100)); // 62/06


// linkSlider2('charger', 'current-charger_status', debounce(bind_trailing_args(updateOBDValue, 'charger'), 100));
// linkSlider('speed', 'vehicle-speed', debounce(bind_trailing_args(updateOBDValue, 'speed'), 100));
// linkSlider1('assist', 'assist-level', debounce(bind_trailing_args(updateOBDValue, 'assist'), 100));
// linkSlider('distance', 'remaining-distance', debounce(bind_trailing_args(updateOBDValue, 'distance'), 100));
// linkSlider('temperature1', 'current-temperature1', debounce(bind_trailing_args(updateOBDValue, 'temperature1'), 100));
// linkSlider('interfaasi', 'interfaasi-versio', debounce(bind_trailing_args(updateOBDValue, 'interfaasi'),100));