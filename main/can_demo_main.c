#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"
#include "esp_vfs_fat.h"
#include "pcf8574.h"
#include "i2c_rw.h"

#include "CAN.h"
#include "CAN_config.h"

#include "obd.h"

#include <string.h>
#include "http_server.h"

#include <dirent.h>
#include "fs.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"

#ifndef CONFIG_ESPCAN
#error for this demo you must enable and configure ESPCan in menuconfig
#endif

#ifdef CONFIG_CAN_SPEED_100KBPS
#define CONFIG_SELECTED_CAN_SPEED CAN_SPEED_100KBPS
#endif

#ifdef CONFIG_CAN_SPEED_125KBPS
#define CONFIG_SELECTED_CAN_SPEED CAN_SPEED_125KBPS
#endif

#ifdef CONFIG_CAN_SPEED_250KBPS
#define CONFIG_SELECTED_CAN_SPEED CAN_SPEED_250KBPS
#endif

#ifdef CONFIG_CAN_SPEED_500KBPS
#define CONFIG_SELECTED_CAN_SPEED CAN_SPEED_500KBPS
#endif

#ifdef CONFIG_CAN_SPEED_800KBPS
#define CONFIG_SELECTED_CAN_SPEED CAN_SPEED_800KBPS
#endif

#ifdef CONFIG_CAN_SPEED_1000KBPS
#define CONFIG_SELECTED_CAN_SPEED CAN_SPEED_1000KBPS
#endif

#ifdef CONFIG_CAN_SPEED_USER_KBPS
#define CONFIG_SELECTED_CAN_SPEED CONFIG_CAN_SPEED_USER_KBPS_VAL /* per menuconfig */
#endif
// #define CAN_RESPONSE_IDENTIFIER 0x7E8
// #define CAN_SEND_IDENTIFIER 0x7DF
#define CAN_RESPONSE_IDENTIFIER 0x701
#define CAN_SEND_IDENTIFIER 0x700

//system status
#define BATTERY_HEATING_ON          0
#define BATTERY_COOLING_ON          1
#define BATTERY_CYCLE_TEST_ON       2
#define STORAGE_SWITCH_ON           3
#define REBOOTED                    4
#define AC_GRID_OFF                 5
#define BATTERY_CONNECTED           6
#define OP_STATE_NORMAL             8
#define OP_STATE_UPS                9
#define LED1 						5

#define GW_RESET_PERIOD 15*60*1000

#ifdef CONFIG_CAN_SPEED_250KBPS
uint8_t canSendDelay1 = 24;
uint8_t canSendDelay2 = 168;
#else
uint8_t canSendDelay1 = 6;
uint8_t canSendDelay2 = 42;
#endif
int64_t canSendDelayUSec = 10;
xSemaphoreHandle timerSemaphore;

CAN_device_t CAN_cfg = {
	.speed = CONFIG_SELECTED_CAN_SPEED,		 // CAN Node baudrade
	.tx_pin_id = CONFIG_ESP_CAN_TXD_PIN_NUM, // CAN TX pin example menuconfig GPIO_NUM_5
	.rx_pin_id = CONFIG_ESP_CAN_RXD_PIN_NUM, // CAN RX pin example menuconfig GPIO_NUM_35 ( Olimex )
	.rx_queue = NULL,						 // FreeRTOS queue for RX frames
};

// Queue for CAN multi-frame packets
uint8_t can_flow_queue[5][8];

unsigned int assist_level = 1;
unsigned int battery_capacity = 90;
int battery_charger_status = 1;
float battery_current = 50;
float battery_temp = 21;
float battery_temp1 = 22;
uint8_t battery_type = 2;
float battery_voltage = 4.15;
unsigned int bldc_status = 1;
float bldc_temp = 55;
float charging_current = 25;
float drive_fw_version = 1.24;
float max_battery_capacity = 1000;
float max_charger_current = 100;
unsigned int max_speed = 25;
float motor_power = 20;
float motor_power_pros = 30;
float motor_rpm = 2000;
unsigned int motor_status = 1;
float motor_temp = 50;
float odo_31 = 23465;
unsigned int power_status = 1;
unsigned int rajapinta_versio = 5;
float remaining_charging_time = 324;
float remaining_distance = 1123;
unsigned int reset_odo = 0;
unsigned int reset_trip = 0;
long rpm_ratio = 1067316150; // IEEE754: 1.234
float trip_31 =320;
float vehicle_rpm = 3000;
unsigned int vehicle_speed = 20;
float vehicle_throttle = 10;
char vehicle_vin[17] = "L7DRIVE_SIMULATOR";
float voltage_U24 = 24;
float wheel_rot_time = 1;
unsigned int wheel_size_code = 20;

// lux

uint8_t luxBatteryLevel =75; 			// 0x60 0x01, 0..100%, SoC
uint16_t luxBatteryCapacity = 750;  	// 0x60 0x02, pitäis olla 0..2000Wh
uint16_t luxBatteryCurrent =-36; 		// 0x60 0x03, 0..300A
float luxBatteryVoltage = 3.25;			// 0x60 0x04, 2.8..4.5V
float luxOutputVoltage = 48; 			// 0x60 0x05, 40..60V, nominal 48V
int8_t luxBatteryTemperature = 21; 		// 0x60 0x06, -100..100deg
uint8_t luxHeatingPower = 0;			// 0x60 0x07, 0..100W. 39W nominal (48V)
int16_t luxBoosterPower = -106; 		// 0x60 0x08, -1500..1500W
uint16_t luxInputPowerFromGrid = 600;  	// 0x60 0x09, 0..1500W (48V)
uint16_t luxTotalOutputPower = 494; 	// 0x60 0x0A, 0..1500W
uint16_t luxSystemStatus = 0xC0; 		// 0x60 0x0B, status mask
int8_t luxBcsTemperature = 39;   		// 0x60 0x0C, -100..100deg
uint8_t luxBatterySoH = 80;				// 0x60 0x0D, 0..100%. Battery state of health
int8_t luxuCELL1Temp = 35;				// 0x60 0x0E, -100..100deg
int8_t luxuCELL2Temp = 36;				// 0x60 0x0F, -100..100deg
int8_t luxuCELL3Temp = 37;				// 0x60 0x10, -100..100deg
float luxFwVersion = 1.23;  			// 0x60 0x11
uint32_t luxSerialNbrHigh = 0x41424344;	// 0x60 0x12
uint32_t luxSerialNbrLow =  0x31323334; // 0x60 0x13
// uint64_t luxSerialNbr = 12000459802334;


bool luxbatteryCycleTst = 0;			// 0x61 0x01
int16_t luxMaxInputFromGrid = 600;		// 0x61 0x02 / 0x62 0x01, W
uint16_t luxMaxBatteryChargingPower = 200; // 0x61 0x03 / 0x62 0x02, W
uint8_t luxMinChargeLevel = 30;  		// 0x61 0x04 / 0x62 0x03, 0..100%
uint8_t luxMaxChargeLevel = 90; 		// 0x61 0x05 / 0x62 0x04, 0..100%
bool luxNetOnRefresh = 1; 				// 0x61 0x06, 0 = Off, 1 = On
uint16_t luxEnergyFromGrid = 3184/30;		// 0x62 0x05, Ws
int32_t luxEnergyFromBatt = -563/30;		// 0x62 0x06, Ws

uint32_t lastUpdateGridETicks = 0;
uint16_t luxEnergyFromGridUp = 110;
uint32_t lastUpdateBattETicks = 0;
int32_t luxEnergyFromBattUp = 100;
uint32_t nowTimeTicks;
uint32_t dataSent = 0;
uint32_t dataSentCheck = 0;




int lastTestState = 0;
static EventGroupHandle_t wifi_event_group;

uint32_t allDataSentTime = 0;
uint32_t startTicks = 0;

#define WIFI_SSID "L7Energy-Simulator-new-pids-v1"
#define WIFI_PASS "11111111"

void IRAM_ATTR timer_group0_isr (){
    // ets_printf("\n----- hep ----\n");
    //ets_printf("led timer iram0 isr got interrupt. Timergroup: %d, Timer_no: %d, led no: %d, led_cmd %d\n\n", isrControl.timer_group, isrControl.timer_no, isrControl.led_no, isrControl.led_command);

	static BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    TIMERG0.int_clr_timers.t0 = 1; //clear interrupt bit
	TIMERG0.hw_timer[0].config.alarm_en = 1;
	xSemaphoreGiveFromISR(timerSemaphore,&xHigherPriorityTaskWoken);
	if(xHigherPriorityTaskWoken){
		portYIELD_FROM_ISR();
	}
	// timer_set_counter_value(0,0,0);
	// timer_enable_intr(0,0);
    // timer_disable_intr(isrControl.timer_group, isrControl.timer_no);
    // xQueueSendFromISR(led_control_queue, &isrControl, NULL);

}
static int timer_config(){

    //printf("\n------- enter timer config------------\n\n");

    timer_config_t timeConf;
    timeConf.divider = 80; //Set prescaler for clock, 80: 1usec period
    timeConf.counter_dir = TIMER_COUNT_UP;
    timeConf.alarm_en = 1;
    timeConf.intr_type = TIMER_INTR_LEVEL;
    timeConf.auto_reload = TIMER_AUTORELOAD_EN; //
    timeConf.counter_en = TIMER_PAUSE;
    timer_init(0,0,&timeConf); //start timer x at group x
    timer_set_counter_value(0,0,0); //set start value for timer x
    timer_isr_register(0,0, timer_group0_isr,NULL,ESP_INTR_FLAG_IRAM,NULL);

    //printf("timer_no %d, timer_group %d, led_no %d, led cmd %d \n", param->timer_no, param->timer_group, param->led_no, param->led_command );
    // timer_set_alarm_value(param->timer_group,param->timer_no,param->timeMs);
    // timer_enable_intr(param->timer_group,param->timer_no);
    //printf("start timer\n\n");
    timer_set_alarm_value(0,0,canSendDelayUSec);
    timer_enable_intr(0,0);
    timer_start(0,0);
    return 0;
}

void sample_timer_task(){
	// timer_config();
	while(1){
		xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		// printf("hep\n");
		xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		// printf("hop\n");
		xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		vTaskDelay(1000 / portTICK_PERIOD_MS);

		// printf("hyp\n");
	}
}
esp_err_t event_handler(void *ctx, system_event_t *event)
{
	return ESP_OK;
}

CAN_frame_t createOBDResponse(unsigned int mode, unsigned int length)
{
	CAN_frame_t response;

	// 	response.MsgID = CAN_RESPONSE_IDENTIFIER;
	// response.FIR.B.DLC = 8;
	// response.FIR.B.FF = CAN_frame_std;
	// response.FIR.B.RTR = CAN_no_RTR;
	// response.data.u8[0] = 2; // Number of data bytes ()
	// response.data.u8[1] =  0x40 + mode; // Mode (+ 0x40)
	// response.data.u8[2] = pid; // PID
	response.MsgID = mode;
	response.FIR.B.DLC = length;
	response.FIR.B.FF = CAN_frame_std;
	response.FIR.B.RTR = CAN_no_RTR;
	response.data.u8[0] = 0; // Number of data bytes ()
	// response.data.u8[1] =  0x40 + mode; // Mode (+ 0x40)
	// response.data.u8[2] = pid; // PID

	return response;
}

int sendOBDResponse(CAN_frame_t *response)
{
	int success = CAN_write_frame(response);

	// printf("Response = %d | (0x%03x) 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x\n",
		//    success, response->MsgID,
		//    response->data.u8[0], response->data.u8[1], response->data.u8[2], response->data.u8[3],
		//    response->data.u8[4], response->data.u8[5], response->data.u8[6], response->data.u8[7]);

	return success;
}

void respondToOBD1(uint8_t pid)
{
	printf("Responding to Mode 1 PID 0x%02x\n", pid);

	CAN_frame_t response = createOBDResponse(0x1, pid);

	switch (pid)
	{
		case 0x00: // Supported PIDs
			response.data.u8[0] += 4; // Number of data bytes
			response.data.u8[3] = 0x00; // Data byte 1  00
			response.data.u8[4] = 0x18; // Data byte 2  18
			response.data.u8[5] = 0x80; // Data byte 3  80
			response.data.u8[6] = 0x00; // Data byte 4  00
			break;
		case 0x0C: // RPM
			response.data.u8[0] += 2; // Number of data bytes
			obdRevConvert_010C(vehicle_rpm, &response.data.u8[3], &response.data.u8[4], 0, 0);
			//printf("RPM vastaus bytet A: 0x%02x, ja B: 0x%02x\n", response.data.u8[3], response.data.u8[4]]);
			break;
		case 0x0D: // Speed
			//printf("Speed query received\n");
			response.data.u8[0] += 1; // Number of data bytes
			//printf("Speed: %d", vehicle_speed);
			obdRevConvert_010D(vehicle_speed, &response.data.u8[3], 0, 0, 0);
			break;
	}
	sendOBDResponse(&response);
}

void respondToOBD30(uint8_t pid)
{
	printf("Responding to Mode 30 PID 0x%02x\n", pid);

	CAN_frame_t response = createOBDResponse(0x30, pid);

	switch (pid)
	{
		case 0x00: // Supported PIDs
			response.data.u8[0] += 4; // Number of data bytes
			response.data.u8[3] = 0xFF; // Data byte 1
			response.data.u8[4] = 0xF8; // Data byte 2  0xF8
			response.data.u8[5] = 0x00; // Data byte 3  0x00
			response.data.u8[6] = 0x00; // Data byte 4  0x00
			break;
		case 0x01: // Interface version (1-255)
			response.data.u8[0] += 1; // Number of data bytes
			obdRevConvert_3001(rajapinta_versio, &response.data.u8[3], 0, 0, 0);
			break;
		case 0x02: // Battery voltage
			response.data.u8[0] += 2; // Number of data bytes
			obdRevConvert_3002(battery_voltage, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
		case 0x03: // Throttle position
			response.data.u8[0] += 1; // Number of data bytes
			obdRevConvert_3003(vehicle_throttle, &response.data.u8[3], 0, 0, 0);
			break;
		case 0x04: // Motor RPM
			response.data.u8[0] += 2; // Number of data bytes
			obdRevConvert_3004(motor_rpm, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
		case 0x05: // Wheel rotation time
			response.data.u8[0] += 2; // Number of data bytes
			obdRevConvert_3005(wheel_rot_time, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
		case 0x06: // Wheel size code
			response.data.u8[0] += 1; // Number of data bytes
			obdRevConvert_3006(wheel_size_code, &response.data.u8[3], 0, 0, 0);
			break;
		case 0x07: // Wheel rotation time
			response.data.u8[0] += 2; // Number of data bytes
			obdRevConvert_3007(motor_power, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
		case 0x08: // Assist level
			response.data.u8[0] += 1; // Number of data bytes
			obdRevConvert_3008(assist_level, &response.data.u8[3], 0, 0, 0);
			break;
		case 0x09: // Assist level
			response.data.u8[0] += 1; // Number of data bytes
			obdRevConvert_3009(motor_power_pros, &response.data.u8[3], 0, 0, 0);
			break;
		case 0x0A: // Max speed
			response.data.u8[0] += 1; // Number of data bytes
			obdRevConvert_300A(max_speed, &response.data.u8[3], 0, 0, 0);
			break;
		case 0x0B: // Max battery capacity
			response.data.u8[0] += 2; // Number of data bytes
			obdRevConvert_300B(max_battery_capacity, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
		case 0x0C: // Max charger current
			response.data.u8[0] += 1; // Number of data bytes
			obdRevConvert_300C(max_charger_current, &response.data.u8[3],0, 0, 0);
			break;
		case 0x0D: // Battery type
			response.data.u8[0] += 1; // Number of data bytes
			obdRevConvert_300D(battery_type, &response.data.u8[3], 0, 0, 0);
			break;
	}
	sendOBDResponse(&response);

}

void respondToOBD31(uint8_t pid)
{
	printf("Responding to Mode 31 PID 0x%02x\n", pid);

	CAN_frame_t response = createOBDResponse(0x31, pid);

	switch (pid)
	{
		case 0x00: // Supported PIDs
			response.data.u8[0] += 4; // Number of data bytes
			response.data.u8[3] = 0xFF; // Data byte 1  ff
			response.data.u8[4] = 0xFF; // Data byte 2  ff
			response.data.u8[5] = 0x80; // Data byte 3  80
			response.data.u8[6] = 0x00; // Data byte 4  00
			break;
		case 0x01: // battery current
			response.data.u8[0] += 2; // Number of data bytes (added to 2)
			obdRevConvert_3101(battery_current, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
		case 0x02: // charging current
			response.data.u8[0] += 2; // Number of data bytes (added to 2)
			//printf("rivi 191, charg current vastauksen byte määrä %d, ja charging_current arvo %f\n",response.data.u8[0], charging_current );
			obdRevConvert_3102(charging_current, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
		case 0x03: // U24 voltage
			response.data.u8[0] += 2; // Number of data bytes (added to 2)
			obdRevConvert_3103(voltage_U24, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
		case 0x04: // battery temp
			response.data.u8[0] += 1; // Number of data bytes
			obdRevConvert_3104(battery_temp, &response.data.u8[3], 0, 0, 0);
			break;
		case 0x05: // battery temp1
			response.data.u8[0] += 1; // Number of data bytes
			obdRevConvert_3105(battery_temp1, &response.data.u8[3], 0, 0, 0);
			break;
		case 0x06: // Motor temperature
			response.data.u8[0] += 1; // Number of data bytes
			obdRevConvert_3106(motor_temp, &response.data.u8[3], 0, 0, 0);
			break;
		case 0x07: // Bldc temperature
			response.data.u8[0] += 1; // Number of data bytes
			obdRevConvert_3107(bldc_temp, &response.data.u8[3], 0, 0, 0);
			break;
		case 0x0B: // Drive FW version
			response.data.u8[0] += 2; // Number of data bytes (added to 2)
			obdRevConvert_310B(drive_fw_version, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
		case 0x0C: // ODO
			response.data.u8[0] += 4; // Number of data bytes (added to 2)
			obdRevConvert_310C(odo_31, &response.data.u8[3], &response.data.u8[4], &response.data.u8[5], &response.data.u8[6]);
			break;
		case 0x0D: // TRIP
			response.data.u8[0] += 4; // Number of data bytes (added to 2)
			obdRevConvert_310D(trip_31, &response.data.u8[3], &response.data.u8[4], &response.data.u8[5], &response.data.u8[6]);
			break;
		case 0x0E: // Charger status
			response.data.u8[0] += 4; // Number of data bytes (added to 2)
			obdRevConvert_310E(battery_charger_status, &response.data.u8[3], &response.data.u8[4], &response.data.u8[5], &response.data.u8[6]);
			break;
		case 0x0F: // battery capacity
			response.data.u8[0] += 1; // Number of data bytes
			obdRevConvert_310F(battery_capacity, &response.data.u8[3], 0, 0, 0);
			break;
		case 0x10: // Remaining distance
			response.data.u8[0] += 2; // Number of data bytes (added to 2)
			obdRevConvert_3110(remaining_distance, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
		case 0x11: // Remaining charging time
			response.data.u8[0] += 2; // Number of data bytes (added to 2)
			obdRevConvert_3111(remaining_charging_time, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
	}

	sendOBDResponse(&response);
}

void respondToOBD32(uint8_t pid, uint8_t data0, uint8_t data1, uint8_t data2, uint8_t data3)
{
	printf("Responding to Mode 32 PID 0x%02x\n", pid);

	CAN_frame_t response = createOBDResponse(0x32, pid);

	printf("D0: 0x%02x, ", data0);
	printf("D1: 0x%02x, ", data1);
	printf("D2: 0x%02x, ", data2);
	printf("D3: 0x%02x, \n", data3);

	switch (pid)
	{
		case 0x00: // Supported PIDs
			response.data.u8[0] += 4; // Number of data bytes
			response.data.u8[3] = 0xFF; // Data byte 1  ff
			response.data.u8[4] = 0xFF; // Data byte 2  ff
			response.data.u8[5] = 0xF0; // Data byte 3  f0
			response.data.u8[6] = 0x00; // Data byte 4  00
			break;
		case 0x01: // Set assist level 1
			assist_level=1;
			break;
		case 0x02: // Set assist level 2
			assist_level=2;
			break;
		case 0x03: // Set assist level 3
			assist_level=3;
			break;
		case 0x04: // Set assist level 4
			assist_level=4;
			break;
		case 0x05: // Set assist level 5
			assist_level=5;
			break;
		case 0x06: // Reset ODO
			odo_31 = 0;
			break;
		case 0x07: // Reset trip
			trip_31 = 0;
			break;
		case 0x08: // Set wheel size code 8
			wheel_size_code = 8;
			break;
		case 0x09: // Set wheel size code 16
			wheel_size_code = 16;
			break;
		case 0x0A: // Set wheel size code 20
			wheel_size_code = 20;
			break;
		case 0x0B: // Set wheel size code 28
			wheel_size_code = 28;
			break;
		case 0x0C: // Set wheel size code 30
			wheel_size_code = 30;
			break;
		case 0x0D: // Set max speed 20
			max_speed = 20;
			break;
		case 0x0E: // Set max speed 25
			max_speed = 25;
			break;
		case 0x0F: // Set max speed 30
			max_speed = 30;
			break;
		case 0x10: // Set max speed 35
			max_speed = 35;
			break;
		case 0x11: // Set max speed 40
			max_speed = 40;
			break;
		case 0x12: // Set max battery capacity
			max_battery_capacity = 256*data0+data1;
			break;
		case 0x13: // Set max charger current
			printf("data0: %d, data1: %d, max_charger_current: %f\n", data0, data1, max_charger_current);
			max_charger_current = data0;
			printf("data0: %d, data1: %d, max_charger_current: %f\n", data0, data1, max_charger_current);
			break;
		case 0x14: // Set battery type
			battery_type = data0;

			break;
	}
	sendOBDResponse(&response);
}
void respondToOBD33(uint8_t pid, uint8_t data0, uint8_t data1, uint8_t data2, uint8_t data3)
{
	CAN_frame_t response = createOBDResponse(0x33, pid);

	switch (pid)
	{
		case 0x00: // Supported PIDs
			response.data.u8[0] += 4; // Number of data bytes
			response.data.u8[3] = 0xFF; // Data byte 1  ff
			response.data.u8[4] = 0xFF; // Data byte 2  ff
			response.data.u8[5] = 0xF0; // Data byte 3  f0
			response.data.u8[6] = 0x00; // Data byte 4  00
			break;
		case 0x01: // Get RPM ratio
			response.data.u8[0] += 4; // Number of data bytes (added to 2)
			obdRevConvert_3301(rpm_ratio, &response.data.u8[3], &response.data.u8[4], &response.data.u8[5], &response.data.u8[6]);
			break;
		case 0x02: // Set RPM ratio
			rpm_ratio = (data0 << 24) | (data1 << 16) | (data2 << 8) | data3;
			break;
	}

	sendOBDResponse(&response);
}
void respondToOBD60(uint8_t pid)
{
	// printf("Responding to Mode 60 PID 0x%02x\n", pid);

	CAN_frame_t response = createOBDResponse(0x60, pid);

	switch (pid)
	{
		case 0x00: // Supported PIDs
			response.data.u8[0] += 4; // Number of data bytes
			response.data.u8[3] = 0xFF; // Data byte 1
			response.data.u8[4] = 0xFF; // Data byte 2
			response.data.u8[5] = 0xE0; // Data byte 3
			response.data.u8[6] = 0x00; // Data byte 4
			break;
		case 0x01: // Battery level (SoC)
			response.data.u8[0] += 1; // Number of data bytes (added to 2)
			obdRevConvert_6001(luxBatteryLevel, &response.data.u8[3], 0, 0, 0);
			break;
		case 0x02: // Remaining battery, 0..2000Wh
			response.data.u8[0] += 2; // Number of data bytes (added to 2)
			obdRevConvert_6002(luxBatteryCapacity, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
		case 0x03: // Battery current
			response.data.u8[0] += 2; // Number of data bytes (added to 2)
			obdRevConvert_6003(luxBatteryCurrent, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
		case 0x04: // Battery voltage
			response.data.u8[0] += 2; // Number of data bytes
			obdRevConvert_6004(luxBatteryVoltage, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
		case 0x05: // System output voltage
			response.data.u8[0] += 2; // Number of data bytes
			obdRevConvert_6005(luxOutputVoltage, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
		case 0x06: // Battery temperature
			response.data.u8[0] += 1; // Number of data bytes
			obdRevConvert_6006(luxBatteryTemperature, &response.data.u8[3], 0, 0, 0);
			break;
		case 0x07: // Heating power
			response.data.u8[0] += 1; // Number of data bytes
			obdRevConvert_6007(luxHeatingPower, &response.data.u8[3], 0, 0, 0);
			break;
		case 0x08: // Battery charging power --> booster power
			response.data.u8[0] += 2; // Number of data bytes (added to 2)
			obdRevConvert_6008(luxBoosterPower, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
		case 0x09: // BCS Input power --> Input power from grid
			response.data.u8[0] += 2; // Number of data bytes (added to 2)
			obdRevConvert_6009(luxInputPowerFromGrid, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
		case 0x0A: // Total output power
			response.data.u8[0] += 2; // Number of data bytes (added to 2)
			obdRevConvert_600A(luxTotalOutputPower, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
		case 0x0B: // System Status Mask
			response.data.u8[0] += 2; // Number of data bytes (added to 2)
			obdRevConvert_600B(luxSystemStatus, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
		case 0x0C: // L7Drive BCS Temperature
			response.data.u8[0] += 1; // Number of data bytes
			obdRevConvert_600C(luxBcsTemperature, &response.data.u8[3], 0, 0, 0);
			break;
		case 0x0D: // Battery State of Health (SoH)
			response.data.u8[0] += 1; // Number of data bytes (added to 2)
			obdRevConvert_600D(luxBatterySoH, &response.data.u8[3], 0, 0, 0);
			break;
		case 0x0E: // uCELL1 Temperature
			response.data.u8[0] += 1; // Number of data bytes
			obdRevConvert_600E(luxuCELL1Temp, &response.data.u8[3], 0, 0, 0);
			break;
		case 0x0F: // uCELL2 Temperature
			response.data.u8[0] += 1; // Number of data bytes
			obdRevConvert_600F(luxuCELL2Temp, &response.data.u8[3], 0, 0, 0);
			break;
		case 0x10: // uCELL3 Temperature
			response.data.u8[0] += 1; // Number of data bytes
			obdRevConvert_6010(luxuCELL3Temp, &response.data.u8[3], 0, 0, 0);
			break;
		case 0x11: // FW version
			response.data.u8[0] += 2; // Number of data bytes
			obdRevConvert_6011(luxFwVersion, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
		case 0x12: // Serian Number High Word
			response.data.u8[0] += 4; // Number of data bytes
			obdRevConvert_6012(luxSerialNbrHigh, &response.data.u8[3], &response.data.u8[4], &response.data.u8[5], &response.data.u8[6]);
			break;
		case 0x13: // Serian Number Low Word
			response.data.u8[0] += 4; // Number of data bytes
			obdRevConvert_6013(luxSerialNbrLow, &response.data.u8[3], &response.data.u8[4], &response.data.u8[5], &response.data.u8[6]);
			break;


	}
	sendOBDResponse(&response);
}

void respondToOBD61(uint8_t pid, uint8_t data0, uint8_t data1, uint8_t data2, uint8_t data3)
{
	// printf("Responding to Mode 61 PID 0x%02x\n", pid);

	CAN_frame_t response = createOBDResponse(0x61, pid);

	switch (pid)
	{
		case 0x00: // Supported PIDs
			response.data.u8[0] += 4; // Number of data bytes
			response.data.u8[3] = 0xFC; // Data byte 1
			response.data.u8[4] = 0x00; // Data byte 2
			response.data.u8[5] = 0x00; // Data byte 3
			response.data.u8[6] = 0x00; // Data byte 4
			break;
		case 0x01: // Set batteryCycleTst state
			if (data0==1){
				gpio_set_level(LED1,1);
				luxbatteryCycleTst=1;
				luxSystemStatus|= 1UL << BATTERY_CYCLE_TEST_ON;
			}
			else {
				gpio_set_level(LED1,0);
				luxbatteryCycleTst=0;
				luxSystemStatus &= ~(1UL << BATTERY_CYCLE_TEST_ON);
			}
			printf("got betterycycletest: %d\n\n",luxbatteryCycleTst);
			break;

		case 0x02: // Set Maximum Input Grid Power
			luxMaxInputFromGrid = 256*data0+data1;
			printf("set luxMaxInputFromGrid: %d\n\n",luxMaxInputFromGrid);
			    //temporary test-------------------------------
			gpio_set_level(LED1,lastTestState);
			if (lastTestState ==0) lastTestState =1;
			else lastTestState =0;
			//temporary test end-------------------
			break;
		case 0x03: // Set Maximum Battery Charging Power
			luxMaxBatteryChargingPower=256*data0+data1;
			break;
		case 0x04: // Set Min charge level
			luxMinChargeLevel = data0;
			break;
		case 0x05: // Set Max charge level
			luxMaxChargeLevel = data0;
			break;
		case 0x06: // NetOn refresh
			luxNetOnRefresh = data0;
			break;
	}
	sendOBDResponse(&response);
}

void respondToOBD62(uint8_t pid, uint8_t data0, uint8_t data1, uint8_t data2, uint8_t data3)
{
	// printf("Responding to Mode 62 PID 0x%02x\n", pid);

	CAN_frame_t response = createOBDResponse(0x62, pid);

	switch (pid)
	{
		case 0x00: // Supported PIDs
			response.data.u8[0] += 4; // Number of data bytes
			response.data.u8[3] = 0xFC; // Data byte 1
			response.data.u8[4] = 0x00; // Data byte 2
			response.data.u8[5] = 0x00; // Data byte 3
			response.data.u8[6] = 0x00; // Data byte 4
			break;
		case 0x01: // Get Maximum Input Grid Power
			response.data.u8[0] += 2; // Number of data bytes (added to 2)
			obdRevConvert_6201(luxMaxInputFromGrid, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
		case 0x02: // Get Maximum Battery Charging Power
			response.data.u8[0] += 2; // Number of data bytes (added to 2)
			obdRevConvert_6202(luxMaxBatteryChargingPower, &response.data.u8[3], &response.data.u8[4], 0, 0);
			break;
		case 0x03: // Get min charge level
			response.data.u8[0] += 1; // Number of data bytes
			obdRevConvert_6203(luxMinChargeLevel, &response.data.u8[3], 0, 0, 0);
			break;
		case 0x04: // Get max charge level
			response.data.u8[0] += 1; // Number of data bytes
			obdRevConvert_6204(luxMaxChargeLevel, &response.data.u8[3], 0, 0, 0);
			break;
		case 0x05: // Get Elapsed AC Grid Power
			response.data.u8[0] += 4; // Number of data bytes
			//targetTickCnt = xTaskGetTickCount() + pdMS_TO_TICKS(tmpOp->interval);
			nowTimeTicks = xTaskGetTickCount();

			luxEnergyFromGridUp = luxEnergyFromGrid * (nowTimeTicks - lastUpdateGridETicks)/100;
			obdRevConvert_6205(luxEnergyFromGridUp, &response.data.u8[3], &response.data.u8[4], &response.data.u8[5], &response.data.u8[6]);
			lastUpdateGridETicks = nowTimeTicks;
			break;
		case 0x06: // Get Elapsed Battery Power
			response.data.u8[0] += 4; // Number of data bytes
			nowTimeTicks = xTaskGetTickCount();
			// printf("\n\n==============================================================================\n");
			// printf("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n\n");
			luxEnergyFromBattUp = luxEnergyFromBatt * ((nowTimeTicks - lastUpdateBattETicks)/100);
			// printf("luxEnergyFromBattUp: %d, luxEnergyFromBatt: %d\n",luxEnergyFromBattUp,luxEnergyFromBatt);
			// printf ("nowTimeTicks: %d, lastUpdateBattETicks: %d\n\n", nowTimeTicks, lastUpdateBattETicks);
			obdRevConvert_6206(luxEnergyFromBattUp, &response.data.u8[3], &response.data.u8[4], &response.data.u8[5], &response.data.u8[6]);
			lastUpdateBattETicks = nowTimeTicks;

			break;
	}
	sendOBDResponse(&response);
}

void sendOBD(uint16_t service)
{
	// printf("Responding to Mode 60 PID 0x%02x\n", pid);

	CAN_frame_t response;
	// xSemaphoreTake(timerSemaphore,portMAX_DELAY);


	switch (service)
	{
		case 5:
			response = createOBDResponse(service, 6);
			response.FIR.B.DLC = 6;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			break;
		case 64:
			response = createOBDResponse(service, 8); // pack voltage. 1*256+121 = 37.7V (7|16@0+, 2 byteä bigendian alusta)
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 1;
			response.data.u8[1] = 121;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 39;
			response.data.u8[5] = 15;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 65:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 17;
			response.data.u8[1] = 24;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 3;
			response.data.u8[5] = 33;
			response.data.u8[6] = 7;
			response.data.u8[7] = 2;
			break;
		case 66:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 64;
			response.data.u8[1] = 40;
			response.data.u8[2] = 45;
			response.data.u8[3] = 8;
			response.data.u8[4] = 16;
			response.data.u8[5] = 67;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 69: //
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 2;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 4;
			response.data.u8[4] = 7;
			response.data.u8[5] = 2;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 80:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 240;
			response.data.u8[1] = 192;
			response.data.u8[2] = 0;
			response.data.u8[3] = 1;
			response.data.u8[4] = 8;
			response.data.u8[5] = 8;
			response.data.u8[6] = 128;
			response.data.u8[7] = 0;
			break;
		case 256:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 257:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 1;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 258:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 17;
			response.data.u8[1] = 22;
			response.data.u8[2] = 17;
			response.data.u8[3] = 13;
			response.data.u8[4] = 17;
			response.data.u8[5] = 24;
			response.data.u8[6] = 17;
			response.data.u8[7] = 18;
			break;
		case 259:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 17;
			response.data.u8[1] = 22;
			response.data.u8[2] = 17;
			response.data.u8[3] = 19;
			response.data.u8[4] = 17;
			response.data.u8[5] = 18;
			response.data.u8[6] = 17;
			response.data.u8[7] = 18;
			break;
		case 260:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 17;
			response.data.u8[1] = 15;
			response.data.u8[2] = 17;
			response.data.u8[3] = 20;
			response.data.u8[4] = 17;
			response.data.u8[5] = 19;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 262:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 1;
			response.data.u8[1] = 255;
			response.data.u8[2] = 192;
			response.data.u8[3] = 0;
			response.data.u8[4] = 15;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 263:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 266:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 63;
			response.data.u8[1] = 63;
			response.data.u8[2] = 63;
			response.data.u8[3] = 63;
			response.data.u8[4] = 63;
			response.data.u8[5] = 62;
			response.data.u8[6] = 63;
			response.data.u8[7] = 64;
			break;
		case 267:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 49;
			response.data.u8[1] = 63;
			response.data.u8[2] = 63;
			response.data.u8[3] = 63;
			response.data.u8[4] = 63;
			response.data.u8[5] = 63;
			response.data.u8[6] = 64;
			response.data.u8[7] = 67;
			break;
		case 268:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 63;
			response.data.u8[1] = 63;
			response.data.u8[2] = 63;
			response.data.u8[3] = 63;
			response.data.u8[4] = 64;
			response.data.u8[5] = 64;
			response.data.u8[6] = 64;
			response.data.u8[7] = 64;
			break;
		case 269:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 64;
			response.data.u8[1] = 64;
			response.data.u8[2] = 64;
			response.data.u8[3] = 64;
			response.data.u8[4] = 64;
			response.data.u8[5] = 64;
			response.data.u8[6] = 64;
			response.data.u8[7] = 0;
			break;
		case 272:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 240;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 273:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 274:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 275:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 276:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 278:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 279:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 282:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 40;
			response.data.u8[1] = 40;
			response.data.u8[2] = 40;
			response.data.u8[3] = 40;
			response.data.u8[4] = 40;
			response.data.u8[5] = 40;
			response.data.u8[6] = 40;
			response.data.u8[7] = 40;
			break;
		case 283:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 40;
			response.data.u8[1] = 40;
			response.data.u8[2] = 40;
			response.data.u8[3] = 40;
			response.data.u8[4] = 40;
			response.data.u8[5] = 40;
			response.data.u8[6] = 40;
			response.data.u8[7] = 40;
			break;
		case 284:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 40;
			response.data.u8[1] = 40;
			response.data.u8[2] = 40;
			response.data.u8[3] = 40;
			response.data.u8[4] = 40;
			response.data.u8[5] = 40;
			response.data.u8[6] = 40;
			response.data.u8[7] = 40;
			break;
		case 285:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 40;
			response.data.u8[1] = 40;
			response.data.u8[2] = 40;
			response.data.u8[3] = 40;
			response.data.u8[4] = 40;
			response.data.u8[5] = 40;
			response.data.u8[6] = 40;
			response.data.u8[7] = 0;
			break;
		case 288:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 289:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 290:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 291:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 292:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 294:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 295:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 298:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 40;
			response.data.u8[1] = 40;
			response.data.u8[2] = 40;
			response.data.u8[3] = 40;
			response.data.u8[4] = 40;
			response.data.u8[5] = 40;
			response.data.u8[6] = 40;
			response.data.u8[7] = 40;
			break;
		case 299:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 40;
			response.data.u8[1] = 40;
			response.data.u8[2] = 40;
			response.data.u8[3] = 40;
			response.data.u8[4] = 40;
			response.data.u8[5] = 40;
			response.data.u8[6] = 40;
			response.data.u8[7] = 40;
			break;
		case 300:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 40;
			response.data.u8[1] = 40;
			response.data.u8[2] = 40;
			response.data.u8[3] = 40;
			response.data.u8[4] = 40;
			response.data.u8[5] = 40;
			response.data.u8[6] = 40;
			response.data.u8[7] = 40;
			break;
		case 301:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 40;
			response.data.u8[1] = 40;
			response.data.u8[2] = 40;
			response.data.u8[3] = 40;
			response.data.u8[4] = 40;
			response.data.u8[5] = 40;
			response.data.u8[6] = 40;
			response.data.u8[7] = 0;
			break;
		case 304:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 240;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 305:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 306:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 307:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 308:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 310:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 311:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 0;
			response.data.u8[1] = 0;
			response.data.u8[2] = 0;
			response.data.u8[3] = 0;
			response.data.u8[4] = 0;
			response.data.u8[5] = 0;
			response.data.u8[6] = 0;
			response.data.u8[7] = 0;
			break;
		case 314:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 40;
			response.data.u8[1] = 40;
			response.data.u8[2] = 40;
			response.data.u8[3] = 40;
			response.data.u8[4] = 40;
			response.data.u8[5] = 40;
			response.data.u8[6] = 40;
			response.data.u8[7] = 40;
			break;

		case 315:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 40;
			response.data.u8[1] = 40;
			response.data.u8[2] = 40;
			response.data.u8[3] = 40;
			response.data.u8[4] = 40;
			response.data.u8[5] = 40;
			response.data.u8[6] = 40;
			response.data.u8[7] = 40;
			break;
		case 316:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 40;
			response.data.u8[1] = 40;
			response.data.u8[2] = 40;
			response.data.u8[3] = 40;
			response.data.u8[4] = 40;
			response.data.u8[5] = 40;
			response.data.u8[6] = 40;
			response.data.u8[7] = 40;
			break;
		case 317:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 40;
			response.data.u8[1] = 40;
			response.data.u8[2] = 40;
			response.data.u8[3] = 40;
			response.data.u8[4] = 40;
			response.data.u8[5] = 40;
			response.data.u8[6] = 40;
			response.data.u8[7] = 40;
			break;
					// ----------------- GW CAN2 data Avant ----------------//
		case 385:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 49;
			response.data.u8[1] = 47;
			response.data.u8[2] = 45;
			response.data.u8[3] = 12;
			response.data.u8[4] = 36;
			response.data.u8[5] = 0;
			response.data.u8[6] = 27;
			response.data.u8[7] = 255;
			break;
		case 386:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 50;
			response.data.u8[1] = 48;
			response.data.u8[2] = 46;
			response.data.u8[3] = 12;
			response.data.u8[4] = 36;
			response.data.u8[5] = 26;
			response.data.u8[6] = 0;
			response.data.u8[7] = 85;
			break;
		case 545:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 77;
			response.data.u8[1] = 01;
			response.data.u8[2] = 201;
			response.data.u8[3] = 00;
			response.data.u8[4] = 110;
			response.data.u8[5] = 0;
			response.data.u8[6] = 85;
			response.data.u8[7] = 8;
			break;
		case 641:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 49;
			response.data.u8[1] = 63;
			response.data.u8[2] = 63;
			response.data.u8[3] = 63;
			response.data.u8[4] = 63;
			response.data.u8[5] = 63;
			response.data.u8[6] = 64;
			response.data.u8[7] = 67;
			break;
		case 769:
			response = createOBDResponse(service, 1);
			response.FIR.B.DLC = 1;
			response.data.u8[0] = 49;
		case 897:
			response = createOBDResponse(service, 8);
			response.FIR.B.DLC = 8;
			response.data.u8[0] = 49;
			response.data.u8[1] = 63;
			response.data.u8[2] = 63;
			response.data.u8[3] = 63;
			response.data.u8[4] = 63;
			response.data.u8[5] = 63;
			response.data.u8[6] = 64;
			response.data.u8[7] = 67;
		case 898:
			response = createOBDResponse(service, 6);
			response.FIR.B.DLC = 6;
			response.data.u8[0] = 49;
			response.data.u8[1] = 63;
			response.data.u8[2] = 63;
			response.data.u8[3] = 63;
			response.data.u8[4] = 63;
			response.data.u8[5] = 63;
			break;
		case 1025:
			response = createOBDResponse(service, 2);
			response.FIR.B.DLC = 2;
			response.data.u8[0] = 49;
			response.data.u8[1] = 63;
			break;
		default:
			break;
	}
	dataSent = xTaskGetTickCount();
	// printf("Line %d\n",__LINE__);
	sendOBDResponse(&response);

}
void task_CAN(void *pvParameters)
{
	(void)pvParameters;

	//frame buffer
	CAN_frame_t __RX_frame;

	//create CAN RX Queue
	CAN_cfg.rx_queue = xQueueCreate(1, sizeof(CAN_frame_t)); // jono oli 10

	//start CAN Module
	CAN_init();
	printf("CAN initialized...\n");

	while (1)
	{
		dataSent = xTaskGetTickCount();
		while(1){
			dataSentCheck = xTaskGetTickCount();
			if ((dataSentCheck - dataSent) > 1000*60) abort();
			printf("time from last sent %d\n", (dataSentCheck-dataSent));
            vTaskDelay(10000 / portTICK_RATE_MS);
		}
		//receive next CAN frame from queue
		if (xQueueReceive(CAN_cfg.rx_queue, &__RX_frame, 20 * portTICK_PERIOD_MS) == pdTRUE)
		{
			/*
			printf("\nFrame from : 0x%08x, DLC %d, RTR %d, FF %d \n", __RX_frame.MsgID, __RX_frame.FIR.B.DLC,
				   __RX_frame.FIR.B.RTR, __RX_frame.FIR.B.FF);
			printf("D0: 0x%02x, ", __RX_frame.data.u8[0]);
			printf("D1: 0x%02x, ", __RX_frame.data.u8[1]);
			printf("D2: 0x%02x, ", __RX_frame.data.u8[2]);
			printf("D3: 0x%02x, ", __RX_frame.data.u8[3]);
			printf("D4: 0x%02x, ", __RX_frame.data.u8[4]);
			printf("D5: 0x%02x, ", __RX_frame.data.u8[5]);
			printf("D6: 0x%02x, ", __RX_frame.data.u8[6]);
			printf("D7: 0x%02x\n", __RX_frame.data.u8[7]);
			printf("==============================================================================\n");
			*/
			// Check if frame is OBD query for L7 (0x7DF)
			if (__RX_frame.MsgID == CAN_SEND_IDENTIFIER) {
				// printf("OBD QUERY ! 0x%02x\n",__RX_frame.data.u8[1] );

				switch (__RX_frame.data.u8[1]) { // Mode

					case 0x01: // Service
						respondToOBD1(__RX_frame.data.u8[2]); //pid
						break;
					case 0x30:
						respondToOBD30(__RX_frame.data.u8[2]);
						break;
					case 0x31:
						respondToOBD31(__RX_frame.data.u8[2]);
						break;
					case 0x32:
						respondToOBD32(__RX_frame.data.u8[2], __RX_frame.data.u8[3], __RX_frame.data.u8[4], __RX_frame.data.u8[5], __RX_frame.data.u8[6]);
						break;
					case 0x33:
						respondToOBD33(__RX_frame.data.u8[2], __RX_frame.data.u8[3], __RX_frame.data.u8[4], __RX_frame.data.u8[5], __RX_frame.data.u8[6]);
						break;
					case 0x60:   // L7Drive Energy specific services
						respondToOBD60(__RX_frame.data.u8[2]);
						break;
					case 0x61:   // L7Drive Energy specific services
						respondToOBD61(__RX_frame.data.u8[2], __RX_frame.data.u8[3], __RX_frame.data.u8[4], __RX_frame.data.u8[5], __RX_frame.data.u8[6]);
						break;
					case 0x62: 	 // L7Drive Energy specific services
						respondToOBD62(__RX_frame.data.u8[2], __RX_frame.data.u8[3], __RX_frame.data.u8[4], __RX_frame.data.u8[5], __RX_frame.data.u8[6]);
						break;
					default:
						printf("Unsupported mode %d !\n", __RX_frame.data.u8[1]);
				}
			}
		}
	}
}
void task_tx_CAN(void *pvParameters)
{
	(void)pvParameters;

	//frame buffer
	CAN_frame_t __RX_frame;

	//create CAN RX Queue
	CAN_cfg.rx_queue = xQueueCreate(1, sizeof(CAN_frame_t)); // jono oli 10

	//start CAN Module
	CAN_init();
	printf("CAN initialized...\n");
	timer_config();
	printf("Line %d\n",__LINE__);


	while (1)
	{
		//send next CAN frame
			/*
			printf("\nFrame from : 0x%08x, DLC %d, RTR %d, FF %d \n", __RX_frame.MsgID, __RX_frame.FIR.B.DLC,
				   __RX_frame.FIR.B.RTR, __RX_frame.FIR.B.FF);
			printf("D0: 0x%02x, ", __RX_frame.data.u8[0]);
			printf("D1: 0x%02x, ", __RX_frame.data.u8[1]);
			printf("D2: 0x%02x, ", __RX_frame.data.u8[2]);
			printf("D3: 0x%02x, ", __RX_frame.data.u8[3]);
			printf("D4: 0x%02x, ", __RX_frame.data.u8[4]);
			printf("D5: 0x%02x, ", __RX_frame.data.u8[5]);
			printf("D6: 0x%02x, ", __RX_frame.data.u8[6]);
			printf("D7: 0x%02x\n", __RX_frame.data.u8[7]);
			printf("==============================================================================\n");
			*/
			// Check if frame is OBD query for L7 (0x7DF)
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);


		// ----------------- GW CAN1 data ----------------//
		sendOBD(65);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(5);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(304);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		// sendOBD(304);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);

		sendOBD(305);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(306);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(315);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		//
		sendOBD(316);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(307);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(308);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(310);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(311);

		vTaskDelay(canSendDelay1 / portTICK_PERIOD_MS);

		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(314);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(317);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(80);

		vTaskDelay(canSendDelay2 / portTICK_PERIOD_MS);

		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(64);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(69);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(5);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(256);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(257);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(267);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(258);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(259);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(260);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(262);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(263);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(266);

		vTaskDelay(canSendDelay1 / portTICK_PERIOD_MS);

		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(268);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(269);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(80);

		vTaskDelay(canSendDelay2 / portTICK_PERIOD_MS);

		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(65);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(64);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(5);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(272);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(273);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(283);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(284);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(274);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(275);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(276);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(278);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(279);

		vTaskDelay(canSendDelay1 / portTICK_PERIOD_MS);

		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(282);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(285);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(80);

		vTaskDelay(canSendDelay2 / portTICK_PERIOD_MS);

		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(66);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(64);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(5);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(288);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(289);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(290);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(291);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(292);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(294);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(295);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(298);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(300);

		vTaskDelay(canSendDelay1 / portTICK_PERIOD_MS);

		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(301);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(80);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(65);

		vTaskDelay(canSendDelay2 / portTICK_PERIOD_MS);

		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(64);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(5);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(304);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(305);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(315);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(306);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(307);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(308);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(310);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(311);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(314);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(316);
		// vTaskDelay(canSendDelay / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		sendOBD(317);

		vTaskDelay(canSendDelay1 / portTICK_PERIOD_MS);
		// xSemaphoreTake(timerSemaphore,portMAX_DELAY);
		// printf("[heap %d]\n", xPortGetFreeHeapSize());
			// printf("Line %d\n",__LINE__);
#ifdef CONFIG_CAN_SPEED_250KBPS
// ----------------- GW CAN1 data ----------------//
		sendOBD(385);
		sendOBD(386);
		sendOBD(545);
		sendOBD(641);
		sendOBD(769);
		sendOBD(897);
		sendOBD(898);
		sendOBD(1025);

// ----------------- GW CAN1 data ----------------//
#endif
		vTaskDelay(canSendDelay1 / portTICK_PERIOD_MS);
		allDataSentTime = xTaskGetTickCount();
		if (allDataSentTime > startTicks + GW_RESET_PERIOD) {
			abort();
		}
	}
}
char *get_filename_ext(const char *filename)
{
    const char *dot = strrchr(filename, '.');
    if(!dot || dot == filename) return "";
    return (char *)dot + 1;
}

char * get_type_for_filename_ext(char *filename)
{
	char *ext = get_filename_ext(filename);
	if (strcmp(ext, "html") == 0) {
		return "text/html";
	} else if (strcmp(ext, "css") == 0) {
		return "text/css";
	} else if (strcmp(ext, "js") == 0) {
		return "text/javascript";
	}
	return NULL;
}

static void cb_GET_file(http_context_t http_ctx, void *ctx)
{
	char *file = malloc(FILE_MAX_SIZE + 1);
	size_t response_size;
	ESP_ERROR_CHECK(readFile((char*)ctx, file, &response_size));

	char *content_type = get_type_for_filename_ext(ctx);

	http_response_begin(http_ctx, 200, content_type, response_size);
	http_buffer_t http_file = {.data = file};
	http_response_write(http_ctx, &http_file);
	free(file);
	http_response_end(http_ctx);
}

static void cb_PATCH_vehicle(http_context_t http_ctx, void* ctx)
{
	const char *name = http_request_get_arg_value(http_ctx, "name");
	const char *value = http_request_get_arg_value(http_ctx, "value");
	unsigned int code = 200;

	if (name != NULL && value != NULL) {
		printf("549 Received %s = %s\n", name, value);
		printf("550 (int)strtof(value,NULL): %d\n", (int)strtof(value,NULL));
		printf("551 =0 %d\n", ((int)strtof(value,NULL)==0));
		ESP_LOGE("HEAP check", "Free HEAP: %i", xPortGetFreeHeapSize());
		if (strcmp(name, "speed") == 0) {
			vehicle_speed = strtol(value, NULL, 10);
		} else if (strcmp(name, "rpm") == 0) {
			vehicle_rpm = strtol(value, NULL, 10);
		} else if (strcmp(name, "throttle") == 0) {
			vehicle_throttle = strtof(value, NULL);
		} else if (strcmp(name, "interfaasi") == 0) {
			rajapinta_versio = strtof(value, NULL);
		} else if (strcmp(name, "assist") == 0) {
			assist_level = strtof(value, NULL);
			if ((int)strtof(value,NULL) == 6) {
				assist_level = 99;
			}
		} else if (strcmp(name, "charger") == 0) {
			printf("(int)strtof(value,NULL): %d\n", (int)strtof(value,NULL));
			printf("strtof(value,NULL): 0x%02x\n", (int)strtof(value,NULL));
			printf("strtof(value,NULL): %s\n", value);
			if (strcmp(value,"init 0x00")==0){
				battery_charger_status = 0x00;
				printf("init 0x00\n");
			}
			else if (strcmp(value,"charging 0x10")==0){
				battery_charger_status = 0x10;
				printf("charging 0x10\n");
			}
			else if (strcmp(value,"active 0x400")==0){
				battery_charger_status = 0x400;
				printf("active 0x400\n");
			}
			else if (strcmp(value,"chr + active 0x410")==0){
				battery_charger_status = 0x410;
				printf("charging + chrg 0x410\n");
			}
		} else if (strcmp(name, "temperature1") == 0) {
			battery_temp1 = strtof(value, NULL);
		} else if (strcmp(name, "capacity") == 0) {
			battery_capacity = strtof(value, NULL);
		} else if (strcmp(name, "distance") == 0) {
			remaining_distance = strtof(value, NULL);

		// lux turri
		} else if (strcmp(name, "battery_l") == 0) {
			luxBatteryLevel = strtof(value, NULL); 		// 60/01
		} else if (strcmp(name, "capa") == 0) {
			luxBatteryCapacity = strtof(value, NULL); 	// 60/02
		} else if (strcmp(name, "battery_c") == 0) {
			luxBatteryCurrent = strtof(value, NULL); 	// 60/03
		} else if (strcmp(name, "battery_v") == 0) {
			luxBatteryVoltage= strtof(value, NULL); 	// 60/04
		} else if (strcmp(name, "system_v") == 0) {
			luxOutputVoltage= strtof(value, NULL); 		// 60/05
		} else if (strcmp(name, "temperature") == 0) {
			luxBatteryTemperature = strtof(value, NULL); // 60/06
		} else if (strcmp(name, "heating_p") == 0) {
			luxHeatingPower = strtof(value, NULL); 		// 60/07
		} else if (strcmp(name, "booster_p") == 0) {
			luxBoosterPower = strtof(value, NULL); 		// 60/08
		} else if (strcmp(name, "grid_in_p") == 0) {
			luxInputPowerFromGrid = strtof(value, NULL); // 60/09
		} else if (strcmp(name, "total_output_p") == 0) {
			luxTotalOutputPower = strtof(value, NULL); 	// 60/0A

		// 60/0B



		} else if (strcmp(name, "battery_h") == 0) {
			if (strtof(value, NULL) == 1) {
				luxSystemStatus|= 1UL << BATTERY_HEATING_ON;
				printf ("\n\n**************************------------------*******\n");
				printf("battery heating ON, luxsystemstatus %x \n", luxSystemStatus);
			}
			else {
				luxSystemStatus &= ~(1UL << BATTERY_HEATING_ON);
				printf ("\n\n**************************------------------*******\n");
				printf("battery heating OFF,  luxsystemstatus %x  \n", luxSystemStatus);
			}

		} else if (strcmp(name, "battery_coo") == 0) {
			if (strtof(value, NULL) == 1) luxSystemStatus|= 1UL << BATTERY_COOLING_ON;
			else luxSystemStatus &= ~(1UL << BATTERY_COOLING_ON);

		} else if (strcmp(name, "storage_s") == 0) {
			if (strtof(value, NULL) == 1)  luxSystemStatus|= 1UL << STORAGE_SWITCH_ON;
			else luxSystemStatus &= ~(1UL << STORAGE_SWITCH_ON);

		} else if (strcmp(name, "rebooted") == 0) {
			if (strtof(value, NULL) == 1)  luxSystemStatus|= 1UL << REBOOTED;
			else luxSystemStatus &= ~(1UL << REBOOTED);

		} else if (strcmp(name, "ac_grid_o") == 0) {
			if (strtof(value, NULL) == 1)  luxSystemStatus|= 1UL << AC_GRID_OFF;
			else luxSystemStatus &= ~(1UL << AC_GRID_OFF);

		} else if (strcmp(name, "battery_conn") == 0) {
			if (strtof(value, NULL) == 1)  luxSystemStatus|= 1UL << BATTERY_CONNECTED;
			else luxSystemStatus &= ~(1UL << BATTERY_CONNECTED);

		} else if (strcmp(name, "op_state_n") == 0) {
			if (strtof(value, NULL) == 1)  luxSystemStatus|= 1UL << OP_STATE_NORMAL;
			else luxSystemStatus &= ~(1UL << OP_STATE_NORMAL);

		} else if (strcmp(name, "op_state_u") == 0) {
			if (strtof(value, NULL) == 1)  luxSystemStatus|= 1UL << OP_STATE_UPS;
			else luxSystemStatus &= ~(1UL << OP_STATE_UPS);
		} else if (strcmp(name, "op_state_u") == 0) {
			if (strtof(value, NULL) == 1)  luxSystemStatus|= 1UL << OP_STATE_UPS;
			else luxSystemStatus &= ~(1UL << OP_STATE_UPS);

		} else if (strcmp(name, "bcs_t") == 0) {
			luxBcsTemperature = strtof(value, NULL); 	// 60/0C
		} else if (strcmp(name, "battery_soh") == 0) {
			luxBatterySoH = strtof(value, NULL); 		// 60/0D
		} else if (strcmp(name, "ucell1_t") == 0) {
			luxuCELL1Temp = strtof(value, NULL); 		// 60/0E
		} else if (strcmp(name, "ucell2_t") == 0) {
			luxuCELL2Temp = strtof(value, NULL); 		// 60/0F
		} else if (strcmp(name, "ucell3_t") == 0) {
			luxuCELL3Temp = strtof(value, NULL); 		// 60/10

		// } else if (strcmp(name, "maxinput_p") == 0) {
		// 	luxMaxInputFromGrid = strtof(value, NULL); 	// 62/01
		// } else if (strcmp(name, "maxbatterychrg_p") == 0) {
		// 	luxMaxBatteryChargingPower = strtof(value, NULL); 	// 62/02
		} else if (strcmp(name, "min_chrg_l") == 0) {
			luxMinChargeLevel = strtof(value, NULL); 	// 62/03
		} else if (strcmp(name, "max_chrg_l") == 0) {
			luxMaxChargeLevel = strtof(value, NULL); 	// 62/04
		} else if (strcmp(name, "energy_g_w") == 0) {
			luxEnergyFromGrid = strtof(value, NULL); 	// 62/05
		} else if (strcmp(name, "energy_batt_w") == 0) {
			luxEnergyFromBatt = strtof(value, NULL); 	// 62/06
		}
		else if (strcmp(name, "vin") == 0) {
			strncpy(vehicle_vin, value, 17);
		}
	}else {
			printf("Invalid data received !\n");
			code = 400;
	}

	http_response_begin(http_ctx, code, "text/plain", HTTP_RESPONSE_SIZE_UNKNOWN);
	http_buffer_t http_response = { .data = "", .data_is_persistent = true };
	http_response_write(http_ctx, &http_response);
	http_response_end(http_ctx);

}
void wifi_init_softap()
{
	wifi_event_group = xEventGroupCreate();

	tcpip_adapter_init();
	ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL));

	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK(esp_wifi_init(&cfg));
	wifi_config_t wifi_config = {
		.ap = {
			.ssid = WIFI_SSID,
			.ssid_len = strlen(WIFI_SSID),
			.password = WIFI_PASS,
			.max_connection = 2,
			.authmode = WIFI_AUTH_WPA_WPA2_PSK},
	};
	if (strlen(WIFI_PASS) == 0)
	{
		wifi_config.ap.authmode = WIFI_AUTH_OPEN;
	}

	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
	ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &wifi_config));
	ESP_ERROR_CHECK(esp_wifi_start());

	printf("wifi_init_softap finished.SSID:%s password:%s, URL: 192.168.4.1\n", WIFI_SSID, WIFI_PASS);
}

void app_main()
{
	// wait for IDF logs to end
	vTaskDelay(1000 / portTICK_PERIOD_MS);


	ESP_ERROR_CHECK(esp32_i2c_init());
	pcf8574_pinWrite(PCF8574_GPIO_P1, 0);

	printf("CAN RXD PIN NUM: %d\n", CONFIG_ESP_CAN_RXD_PIN_NUM);
	printf("CAN TXD PIN NUM: %d\n", CONFIG_ESP_CAN_TXD_PIN_NUM);
	printf("CAN SPEED      : %d KBit/s\n", CONFIG_SELECTED_CAN_SPEED);
	// printf("luxSerialNbrHigh: %u, luxSerialNbrLow %u\n", luxSerialNbrHigh,luxSerialNbrLow);'
#ifdef CONFIG_CAN_SPEED_USER_KBPS
	printf("kBit/s setting was done by User\n");
#endif
	gpio_pad_select_gpio(LED1);
	gpio_set_direction(LED1, GPIO_MODE_OUTPUT);
	gpio_set_level(LED1,1);
	vTaskDelay(1000 / portTICK_PERIOD_MS);
	gpio_set_level(LED1,0);
	vTaskDelay(1000 / portTICK_PERIOD_MS);
	startTicks = xTaskGetTickCount();



	//Create CAN receive task
	xTaskCreate(&task_CAN, "CAN", 2048, NULL, 5, NULL);
	// xTaskCreate(&task_tx_CAN, "CAN", 2048, NULL, 5, NULL);
	// xTaskCreate(&task_tx_CAN, "task_tx_CAN", 4096, NULL, 5, NULL);

	timerSemaphore = xSemaphoreCreateBinary();
    xSemaphoreGive(timerSemaphore);
	xTaskCreatePinnedToCore(task_tx_CAN, "task_tx_CAN", 4096, NULL, configMAX_PRIORITIES - 1, NULL, 1);


	// xTaskCreatePinnedToCore(sample_timer_task, "sample_timer", 4096, NULL, configMAX_PRIORITIES - 1, NULL, 1);
	///////////////// WIFI

	esp_err_t ret = nvs_flash_init();
	if (ret == ESP_ERR_NVS_NO_FREE_PAGES)
	{
		ESP_ERROR_CHECK(nvs_flash_erase());
		ret = nvs_flash_init();
	}
	ESP_ERROR_CHECK(ret);

#ifdef CONFIG_ESP_WIFI_ENABLED
	printf("Initializing WIFI...\n");
	wifi_init_softap();

	///////////////// HTTP

	printf("Initializing HTTP server...\n");

	http_server_t server;
	http_server_options_t http_options = HTTP_SERVER_OPTIONS_DEFAULT();
	//esp_err_t res;

	ESP_ERROR_CHECK(http_server_start(&http_options, &server));
	ESP_ERROR_CHECK(http_register_handler(server, "/", HTTP_GET, HTTP_HANDLE_RESPONSE, &cb_GET_file, "/spiflash/index.html"));
	ESP_ERROR_CHECK(http_register_handler(server, "/main.css", HTTP_GET, HTTP_HANDLE_RESPONSE, &cb_GET_file, "/spiflash/main.css"));
	ESP_ERROR_CHECK(http_register_handler(server, "/main.js", HTTP_GET, HTTP_HANDLE_RESPONSE, &cb_GET_file, "/spiflash/main.js"));
	ESP_ERROR_CHECK(http_register_form_handler(server, "/api/vehicle", HTTP_PATCH, HTTP_HANDLE_RESPONSE, &cb_PATCH_vehicle, NULL));
#endif
	////////////////// FAT

	esp_vfs_fat_mount_config_t mountConfig;
	wl_handle_t m_wl_handle;
	mountConfig.max_files = 4;
	mountConfig.format_if_mount_failed = false;
	ESP_ERROR_CHECK(esp_vfs_fat_spiflash_mount("/spiflash", "storage", &mountConfig, &m_wl_handle));

	ESP_ERROR_CHECK(dumpDir("/spiflash"));
}
