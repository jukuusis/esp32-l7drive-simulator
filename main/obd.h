// Original from http://svn.icculus.org/*checkout*/obdgpslogger/trunk/src/obdinfo/obdconvertfunctions.h

/* Copyright 2009 Gary Briggs

This file is part of obdgpslogger.

obdgpslogger is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

obdgpslogger is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with obdgpslogger.  If not, see <http://www.gnu.org/licenses/>.
*/

/** \file
  \brief Functions to convert from OBD output to actual values
  Functions declared in this file are named by their OBD Service command
   which is two hex digits. It's easier than coming up with short unique
   names myself. Please refer to obdservicecommands.h to see what's what.
*/

#ifndef __OBD_H
#define __OBD_H

#ifdef __cplusplus
extern "C" {
#endif //  __cplusplus

// http://en.wikipedia.org/wiki/Table_of_OBD-II_Codes

/// All obd conversion functions adhere to this
typedef float (*OBDConvFunc)(unsigned int A, unsigned int B,
	unsigned int C, unsigned int D);

/// All obd reverse conversion functions adhere to this
/// return value is number of values [A,B,C,D] filled.
typedef int (*OBDConvRevFunc)(float val, unsigned int *A, unsigned int *B,
	unsigned int *C, unsigned int *D);

int obdRevConvert_0104    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_0105    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_06_09 (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_010A    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_010B    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_010C    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_010D    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_010E    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_010F    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_0110    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_0111    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_14_1B (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_011F    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_0121    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_0122    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_0123    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_24_2B (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_012C    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_012D    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_012E    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_012F    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_0130    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_0131    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_0132    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_0133    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_34_3B (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_3C_3F (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_0142    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_0143    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_0144    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_0145    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_0146    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_0147_4B (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_014C    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_014D    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_014E    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_0152    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_3001  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_3002  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_3003  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_3004  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_3005  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_3006  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_3007  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_3008  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_3009  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_300A  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_300B  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_300C  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_300D  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_3101  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_3102  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_3103  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_3104  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_3105  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_3106  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_3107  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
// int obdRevConvert_3108  (float val, unsigned int *A, unsigned int *B, unsigned int *C, unsigned int *D);
// int obdRevConvert_3109  (float val, unsigned int *A, unsigned int *B, unsigned int *C, unsigned int *D);
// int obdRevConvert_310A  (float val, unsigned int *A, unsigned int *B, unsigned int *C, unsigned int *D);
int obdRevConvert_310B  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_310C  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_310D  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_310E  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_310F  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_3110  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_3111  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_3301  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);

int obdRevConvert_6001  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_6002  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_6003  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_6004  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_6005  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_6006  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_6007  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_6008  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_6009  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_600A  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_600B  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_600C  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_600D  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_600E  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_600F  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_6010  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_6011  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_6012  (uint32_t val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_6013  (uint32_t val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);

int obdRevConvert_6201  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_6202  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_6203  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_6204  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_6205  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
int obdRevConvert_6206  (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D);
#ifdef __cplusplus
}
#endif //  __cplusplus


#endif // __OBD_H