/* Copyright 2009 Gary Briggs

This file is part of obdgpslogger.

obdgpslogger is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

obdgpslogger is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with obdgpslogger.  If not, see <http://www.gnu.org/licenses/>.
*/

/** \file
  \brief Functions to convert from values back to OBDII output
*/

// http://en.wikipedia.org/wiki/Table_of_OBD-II_Codes

#include <stdint.h>
#include <stdio.h>

int obdRevConvert_0104    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)(255.0f*val/100.0f);
	return 1;
}


int obdRevConvert_0105    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)(val+40);
	return 1;
}


int obdRevConvert_06_09 (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)(128.0f*val/100.0f) + 128;
	return 1;
}


int obdRevConvert_010A    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)(val/3.0f);
	return 1;
}


int obdRevConvert_010B    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)val;
	return 1;
}


int obdRevConvert_010C    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	val = val * 4;
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}


int obdRevConvert_010D    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)val;
	return 1;
}


int obdRevConvert_010E    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)((val + 64.0f) * 2.0f);
	return 1;
}


int obdRevConvert_010F    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)(val+40);
	return 1;
}


int obdRevConvert_0110    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	val = val * 100;
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}


int obdRevConvert_0111    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)(255.0f*val/100.0f);
	return 1;
}


int obdRevConvert_14_1B (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)(val / 0.005f);
	return 1;
}


int obdRevConvert_011F    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}


int obdRevConvert_0121    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}


int obdRevConvert_0122    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	val = val / 0.079f;
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}


int obdRevConvert_0123    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	val = val / 10.0f;
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}


int obdRevConvert_24_2B (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	val = val / 0.0000305f;
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}


int obdRevConvert_012C    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)(255.0f*val/100.0f);
	return 1;
}


int obdRevConvert_012D    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)((val + 100.0f) / 0.78125f);
	return 1;
}


int obdRevConvert_012E    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)(255.0f*val/100.0f);
	return 1;
}


int obdRevConvert_012F    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)(255.0f*val/100.0f);
	return 1;
}


int obdRevConvert_0130    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)val;
	return 1;
}


int obdRevConvert_0131    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}


int obdRevConvert_0132    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	val = val + 8192.0f;
	val = val * 4.0f;
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}


int obdRevConvert_0133    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)val;
	return 1;
}


int obdRevConvert_34_3B (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	val = val / 0.0000305f;
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}


int obdRevConvert_3C_3F (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	val = val + 40.0f;
	val = val * 10.0f;
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}


int obdRevConvert_42    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	val = val * 1000.0f;
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}


int obdRevConvert_0143    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	val = val * 255.0f / 100.0f;
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}


int obdRevConvert_0144    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	val = val / 0.0000305f;
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}


int obdRevConvert_0145    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)(255.0f*val/100.0f);
	return 1;
}


int obdRevConvert_0146    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)(val+40);
	return 1;
}


int obdRevConvert_47_4B (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)(255.0f*val/100.0f);
	return 1;
}


int obdRevConvert_014C    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)(255.0f*val/100.0f);
	return 1;
}


int obdRevConvert_014D    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}


int obdRevConvert_014E    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}


int obdRevConvert_0152    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)(255.0f*val/100.0f);
	return 1;
}

int obdRevConvert_3001    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)val;
	return 1;
}
int obdRevConvert_3002    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	val = val*100;
	*A = (uint8_t)((long) val / 256);
	*B = (uint8_t)((long) val % 256);
	return 2;
}
int obdRevConvert_3003    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)val;
	return 1;
}
int obdRevConvert_3004    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}
int obdRevConvert_3005    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}
int obdRevConvert_3006    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)val;
	return 1;
}
int obdRevConvert_3007    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	val = val + 8000;
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}
int obdRevConvert_3008    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)val;
	return 1;
}
int obdRevConvert_3009    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	val = val + 100;
	*A = (uint8_t)val;
	return 1;
}
int obdRevConvert_300A    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)val;
	return 1;
}
int obdRevConvert_300B    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}
int obdRevConvert_300C    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)val;
	return 1;
}
int obdRevConvert_300D    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)val;
	return 1;
}
int obdRevConvert_3101    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}
int obdRevConvert_3102    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	val = val * 10;
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}
int obdRevConvert_3103    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	val = val * 10;
	printf("value %f\n", val);
	*A = (uint8_t)((long)val / 256);
	*B = (uint8_t)((long)val % 256);
	return 2;
}
int obdRevConvert_3104    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)val;
	return 1;
}
int obdRevConvert_3105    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)val;
	return 1;
}
int obdRevConvert_3106    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)val;
	return 1;
}
int obdRevConvert_3107    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)val;
	return 1;
}
int obdRevConvert_310B    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	val = val*100;
	*A = (uint8_t)((long) val / 256);
	*B = (uint8_t)((long) val % 256);
	return 2;
}
int obdRevConvert_310C    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	// val = val/100; ??
	*A = (uint8_t)((long) val / 16777216);
	*B = (uint8_t)(((long) val % 16777216)/65536);
	*C = (uint8_t)(((long) val % 65536)/256);
	*D = (uint8_t)((long) val % 256);
	return 4;
}
int obdRevConvert_310D    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	// val = val/100; ??
	*A = (uint8_t)((long) val / 16777216);
	*B = (uint8_t)(((long) val % 16777216)/65536);
	*C = (uint8_t)(((long) val % 65536)/256);
	*D = (uint8_t)((long) val % 256);
	return 4;
}
int obdRevConvert_310E    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	// val = val/100; ??
	*A = (uint8_t)((long) val / 16777216);
	*B = (uint8_t)(((long) val % 16777216)/65536);
	*C = (uint8_t)(((long) val % 65536)/256);
	*D = (uint8_t)((long) val % 256);
	return 4;
}
int obdRevConvert_310F    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)val;
	return 1;
}
int obdRevConvert_3110   (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)((long) val / 256);
	*B = (uint8_t)((long) val % 256);
	return 2;
}
int obdRevConvert_3111   (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)((long) val / 256);
	*B = (uint8_t)((long) val % 256);
	return 2;
}
int obdRevConvert_3301   (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)((long) val / 16777216);
	*B = (uint8_t)(((long) val % 16777216)/65536);
	*C = (uint8_t)(((long) val % 65536)/256);
	*D = (uint8_t)((long) val % 256);
	return 4;
}


int obdRevConvert_6001    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)val;
	return 1;
}
int obdRevConvert_6002   (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)((long) val / 256);
	*B = (uint8_t)((long) val % 256);
	return 2;
}
int obdRevConvert_6003   (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	int16_t tmp = (int16_t) val;
	// *A = (uint8_t)((long) val / 256);
	// *B = (uint8_t)((long) val % 256);
	*A = (uint8_t)((tmp & 0xFF00)>>8);
	*B = (uint8_t)((tmp & 0xFF));
	return 2;
}
int obdRevConvert_6004   (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	val = val*100;
	*A = (uint8_t)((long) val / 256);
	*B = (uint8_t)((long) val % 256);
	return 2;
}
int obdRevConvert_6005   (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	val = val*10;
	*A = (uint8_t)((long) val / 256);
	*B = (uint8_t)((long) val % 256);
	return 2;
}
int obdRevConvert_6006    (float val, int8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (int8_t)val;
	return 1;
}
int obdRevConvert_6007    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)val;
	return 1;
}
int obdRevConvert_6008   (float val, int8_t *A, int8_t *B, uint8_t *C, uint8_t *D) {
	// printf("luxchargingpower %f\n", val);
	int16_t tmp = (int16_t) val;
	// if (tmp>=0){
    // 	val = tmp;
    // 	*A = (uint8_t)((long) val / 256);
    // 	*B = (uint8_t)((long) val % 256);
    // }
    // else{
    //     tmp = ((-tmp)^0xFF)+1;
    // 	*A = (uint8_t)((long) tmp / 256)^0xFF;
    // 	*B = (uint8_t)((long) tmp % 256);
    // }
	*A = (uint8_t)((tmp & 0xFF00)>>8);
	*B = (uint8_t)((tmp & 0xFF));
	// printf("luxchargingpower A%u B%u\n", *A, *B);
	return 2;
}
int obdRevConvert_6009   (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)((long) val / 256);
	*B = (uint8_t)((long) val % 256);
	return 2;
}
int obdRevConvert_600A   (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)((long) val / 256);
	*B = (uint8_t)((long) val % 256);
	return 2;
}
int obdRevConvert_600B   (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)((long) val / 256);
	*B = (uint8_t)((long) val % 256);
	return 2;
}
int obdRevConvert_600C    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (int8_t)val;
	return 1;
}
int obdRevConvert_600D    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)val;
	return 1;
}
int obdRevConvert_600E    (float val, int8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (int8_t)val;
	return 1;
}
int obdRevConvert_600F    (float val, int8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (int8_t)val;
	return 1;
}
int obdRevConvert_6010    (float val, int8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (int8_t)val;
	return 1;
}
int obdRevConvert_6011   (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	val = val*100;
	*A = (uint8_t)((long) val / 256);
	*B = (uint8_t)((long) val % 256);
	return 2;
}
int obdRevConvert_6012   (uint32_t val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	uint32_t tmp = (uint32_t) val;
	*A = (uint8_t)((tmp & 0xFF000000)>>24);
	*B = (uint8_t)((tmp & 0xFF0000)>>16);
	*C = (uint8_t)((tmp & 0xFF00)>>8);
	*D = (uint8_t)((tmp & 0xFF));
	// *A = (uint8_t)((long) val / 16777216);
	// *B = (uint8_t)(((long) val % 16777216)/65536);
	// *C = (uint8_t)(((long) val % 65536)/256);
	// *D = (uint8_t)((long) val % 256);

	printf("snrb low A%x B%x C%x D%x, luxSerialNbrLow %u\n", *A, *B, *C, *D, val);

	return 4;
}
int obdRevConvert_6013   (uint32_t val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	uint32_t tmp = (uint32_t) val;
	*A = (uint8_t)((tmp & 0xFF000000)>>24);
	*B = (uint8_t)((tmp & 0xFF0000)>>16);
	*C = (uint8_t)((tmp & 0xFF00)>>8);
	*D = (uint8_t)((tmp & 0xFF));
	// *A = (uint8_t)((long) val / 16777216);
	// *B = (uint8_t)(((long) val % 16777216)/65536);
	// *C = (uint8_t)(((long) val % 65536)/256);
	// *D = (uint8_t)((long) val % 256);
	printf("snrb high A%x B%x C%x D%x, luxSerialNbrHigh %u\n", *A, *B, *C, *D, val);
	return 4;
}


int obdRevConvert_6201   (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	int16_t tmp = (int16_t) val;
	*A = (uint8_t)((tmp & 0xFF00)>>8);
	*B = (uint8_t)((tmp & 0xFF));
	// *A = (uint8_t)((long) val / 256);
	// *B = (uint8_t)((long) val % 256);
	return 2;
}
int obdRevConvert_6202   (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)((long) val / 256);
	*B = (uint8_t)((long) val % 256);
	return 2;
}
int obdRevConvert_6203    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)val;
	return 1;
}
int obdRevConvert_6204    (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	*A = (uint8_t)val;
	return 1;
}
int obdRevConvert_6205   (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	int32_t tmp = (int32_t) val;
	// *A = (uint8_t)((long) val / 256);
	// *B = (uint8_t)((long) val % 256);
	*A = (uint8_t)((tmp & 0xFF000000)>>24);
	*B = (uint8_t)((tmp & 0xFF0000)>>16);
	*C = (uint8_t)((tmp & 0xFF00)>>8);
	*D = (uint8_t)((tmp & 0xFF));
	printf("energygrid A%u B%u C%u D%u\n", *A, *B, *C, *D);
	return 4;
}
int obdRevConvert_6206   (float val, uint8_t *A, uint8_t *B, uint8_t *C, uint8_t *D) {
	int32_t tmp = (int32_t) val;
	// printf("energybatt %d\n", );

		// tmp = ((-tmp)^0xFFFF)+1;
	*A = (uint8_t)((tmp & 0xFF000000)>>24);
	*B = (uint8_t)((tmp & 0xFF0000)>>16);
	*C = (uint8_t)((tmp & 0xFF00)>>8);
	*D = (uint8_t)((tmp & 0xFF));
	printf("energybatt A%u B%u C%u D%u\n", *A, *B, *C, *D);
	return 4;
}